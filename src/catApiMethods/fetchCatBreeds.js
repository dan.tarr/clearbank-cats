import axios from "axios";

const fetchCatBreeds = async () => {
	try {
		return (await axios.get(`https://api.thecatapi.com/v1/breeds`, {
			headers: {
				"x-api-key": process.env.REACT_APP_CATS_API_KEY
			}
		})).data;
	} catch (err) {
		return Promise.reject(err);
	}
};

export default fetchCatBreeds;
