import axios from "axios";

const fetchCatImages = async (perPage, pageNumber, breedId, order) => {
	try {
		let searchParams = new URLSearchParams({
			limit: perPage,
			page: pageNumber,
			breed_id: breedId,
			order
		});

		return (await axios.get(`https://api.thecatapi.com/v1/images/search?${searchParams.toString()}`, {
			headers: {
				"x-api-key": process.env.REACT_APP_CATS_API_KEY
			}
		})).data;
	} catch (err) {
		return Promise.reject(err);
	}
};

export default fetchCatImages;
