import React from "react";
import CatsContainer from "./components/CatsContainer/CatsContainer";
import {SkeletonTheme} from "react-loading-skeleton";

import "tailwindcss/tailwind.css";
import "react-loading-skeleton/dist/skeleton.css";

const App = () => {
	return (
		<SkeletonTheme baseColor="#cccccc" highlightColor="#aaaaaa">
			<CatsContainer/>
		</SkeletonTheme>
	);
};

export default App;
