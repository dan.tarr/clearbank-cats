import React from "react";

const Card = ({children}) => {
	return (
		<div className="py-4 px-8 my-4 bg-gray-300 shadow-lg rounded-lg">
			{children}
		</div>
	);
};

export default Card;
