import React from "react";
import Card from "./Card";

const CatCard = ({url}) => {
	return (
		<Card>
			<img src={url} alt="Cat"/>
		</Card>
	);
};

export default CatCard;
