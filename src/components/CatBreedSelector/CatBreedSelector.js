import React from "react";
import Select from "react-select";

const CatBreedSelector = ({breeds, onChange}) => {
	const breedOptions = breeds.map(breed => {
		return {
			value: breed.id,
			label: breed.name
		};
	});

	return (
		<Select className="z-50" options={breedOptions} onChange={onChange}/>
	);
};

export default CatBreedSelector;
