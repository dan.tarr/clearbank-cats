import React, {useEffect, useState} from "react";
import Card from "../Card/Card";
import CatBreedSelector from "../CatBreedSelector/CatBreedSelector";
import Skeleton from "react-loading-skeleton";
import fetchCatImages from "../../catApiMethods/fetchCatImages";
import CatCard from "../Card/CatCard";
import Button from "../Button/Button";
import Error from "../Error/Error";
import fetchCatBreeds from "../../catApiMethods/fetchCatBreeds";

const CatsContainer = () => {
	const [selectedBreed, setSelectedBreed] = useState(null);
	const [breedsLoading, setBreedsLoading] = useState(true);
	const [breeds, setBreeds] = useState([]);
	const [images, setImages] = useState([]);
	const [imagesPage, setImagesPage] = useState(0);
	const [loadingImages, setLoadingImages] = useState(false);
	const [error, setError] = useState(null);

	const loadImages = async (pageNumber) => {
		setLoadingImages(true);

		let newImages;

		try {
			newImages = await fetchCatImages(
				5,
				pageNumber,
				selectedBreed.id,
				"ASC"
			);
		} catch (err) {
			setError("Something went wrong fetching more cats");
			return;
		}

		setLoadingImages(false);

		// There are 5 per page, so if there are less than that returned, then we have run out of cats
		if (newImages.length < 5) {
			setImages([...images, ...newImages]);
			setError("There are no more cats :(");
			return;
		}

		setError(null);
		setImages([...images, ...newImages]);
	};

	const handleChangeBreed = (newValue) => {
		let breed = breeds.find(x => x.id === newValue.value);

		setImagesPage(0);
		setImages([]);
		setSelectedBreed(breed);
	};

	const handleLoadMoreCats = async () => {
		let newPageNumber = imagesPage + 1;

		setImagesPage(newPageNumber);
		await loadImages(newPageNumber);
	};

	useEffect(() => {
		(async () => {
			if (breeds.length === 0) {
				let catBreeds = await fetchCatBreeds();

				setBreedsLoading(false);
				setBreeds(catBreeds);
			}
		})();
	}, []);

	useEffect(() => {
		(async () => {
			if (!selectedBreed)
				return;

			await loadImages(imagesPage);
		})();
	}, [selectedBreed]);

	return (
		<div className="text-center col-span-2 mt-4 mx-2 pb-8 sm:mx-40 lg:mx-60 2xl:mx-120">
			<Card>
				<p className="text-2xl text-left ml-2 mb-4 font-light">
					Select a Breed
				</p>
				{breedsLoading ?
					<Skeleton height="2rem"/> :
					<CatBreedSelector
						breeds={breeds}
						onChange={handleChangeBreed}
					/>
				}

				<p className="mt-4 text-left font-bold text-xl underline">
					{selectedBreed ? selectedBreed.name : null}
				</p>
				<p className="mt-2 text-left font-normal">
					{selectedBreed ? selectedBreed.description : null}
				</p>
			</Card>

			{images ? images.map(image => {
				return <CatCard key={image.id} {...image}/>;
			}) : ""}

			{loadingImages ?
				Array(2).fill(null).map((_, index) => {
					return <Card key={index}>
						<Skeleton height="20rem"/>
					</Card>;
				}) : ""
			}

			{!error && !loadingImages && images.length > 0 ?
				<Button
					iconUrl="https://cdn.discordapp.com/emojis/801059559573553162.png?size=28"
					text="Load more cats!"
					backgroundColourClass="bg-cyan-400"
					onClick={handleLoadMoreCats}
				/> : ""
			}

			{error && !loadingImages ?
				<Error text={error}/>
				: ""
			}
		</div>
	);
};

export default CatsContainer;
