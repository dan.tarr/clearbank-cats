import React from "react";

const Button = ({text, iconUrl, backgroundColourClass, onClick}) => {
	return (
		<button
			type="button"
			className={`text-white ${backgroundColourClass} font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center mr-2 mb-2`}
			onClick={onClick}
		>
			{iconUrl ?
				<img src={iconUrl} alt="icon" className="mr-2"/> : ""
			}
			<span className="text-lg font-normal">
				{text}
			</span>
		</button>
	);
};

export default Button;
