import React from "react";

const Error = ({text}) => {
	return (
		<div className="border border-2 border-amber-600 mx-auto px-4 py-2 bg-orange-200 rounded-xl">
			<p className="text-red-600 font-bold text-xl">
				{text}
			</p>
		</div>
	);
};

export default Error;
