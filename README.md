# ClearBank Cats

Small React app displaying pictures of various cat breeds from [The Cat API](https://thecatapi.com/) in a social media style layout for the ClearBank technical test.

## Showcase

![](./image_1.png)

![](./image_2.png)

![](./image_3.png)

![](./showcase.gif)
