/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		"./src/**/*.js"
	],
	safelist: [],
	theme: {
		extend: {
			spacing: {
				120: "30rem"
			}
		}
	},
	plugins: []
};
